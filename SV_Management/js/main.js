var dssv = [];

//   lấy json lên khi user load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");
// Convert từ json sang array
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }
  renderDSSV(dssv);
}

function themSinhVien() {
  var sv = layThongTinTuForm();
  dssv.push(sv);
  //   conver array dssv thành json
  var dataJson = JSON.stringify(dssv);
  //   Lưu json xuống
  localStorage.setItem("DSSV_LOCAL", dataJson);

  //   render
  renderDSSV(dssv);
  console.log(`🚀 ~ themSinhVien ~ dssv:`, dssv);
}

function xoaSV(id) {
  // Tìm vị trí
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      viTri = i;
    }
  }
  dssv.splice(viTri, 1);
  renderDSSV(dssv);
}

function suaSV(id) {
  document.getElementById("txtMaSV").disabled = true;
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  if (viTri != -1) {
    showThongTinLenForm(dssv[viTri]);
  }
}

function capNhatSinhVien() {
  document.getElementById("txtMaSV").disabled = false;

  var sv = layThongTinTuForm();
  console.log(`🚀 ~ capNhatSinhVien ~ sv:`, sv);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  if (viTri != -1) {
    dssv[i] = sv;
    renderDSSV(dssv);
    resetForm();
  }
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
